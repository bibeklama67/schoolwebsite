import firebase from "firebase/app";
import "firebase/auth";
import "firebase/firestore";

const firebaseConfig = {
    apiKey: "AIzaSyDAi2Z8x_Uwqa8v2mp9TaMbPlrLpOFnbMQ",
    authDomain: "primeclone-eb47c.firebaseapp.com",
    databaseURL: "https://primeclone-eb47c-default-rtdb.firebaseio.com",
    projectId: "primeclone-eb47c",
    storageBucket: "primeclone-eb47c.appspot.com",
    messagingSenderId: "287801586556",
    appId: "1:287801586556:web:56abf009b215f4deb12af9",
    measurementId: "G-JEKST1E6KY"
};
firebase.initializeApp(firebaseConfig);