import React from 'react'
import { Link } from 'react-router-dom'
import './style.scss'
const Navbar = () => {
    return (
        <nav>
            <div className="menu-items">
                <ul>
                    <li>
                        ABOUT <i className="fa fa-caret-down" aria-hidden="true"></i>
                        <div className="sub-menu-1">
                            <ul>
                                <li>INTRODUCTION
                                </li>
                                <li>MESSAGE
                                <i className="fa fa-caret-right" aria-hidden="true"></i>
                                    <div className="sub-menu-2">
                                        <ul>
                                            <li><Link to="/message-from-principal">MESSAGE FROM PRINCIPAL</Link></li>
                                            <li>MESSAGE FROM PROGRAM DIRECTOR</li>
                                        </ul>
                                    </div>
                                </li>
                                <li>WHY JOIN PRIME</li>
                                <li>RESOURCE AND FACILITIES</li>
                            </ul>
                        </div>
                    </li>
                    <li>ACADEMICS <i className="fa fa-caret-down" aria-hidden="true"></i></li>
                    <li>LIFE AT PRIME <i className="fa fa-caret-down" aria-hidden="true"></i></li>
                    <li>ENROLL NOW</li>
                    <li>CONTACT US <i className="fa fa-caret-down" aria-hidden="true"></i></li>
                    <div className="ham-menu"><i className="fa fa-bars"></i></div>
                </ul>

            </div>
        </nav>
    )
}
export default Navbar