import React from 'react'
import Navbar from '../navbar'
import InfoSection from './components/InfoSection'
import TopBar from './components/TopBar'
import './style.scss'
const Header = () => {
    return (
        <>
            <TopBar />
            <InfoSection />
            <Navbar />
        </>
    )
}
export default Header