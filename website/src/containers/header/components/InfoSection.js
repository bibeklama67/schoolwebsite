import React from 'react'
import { Link } from 'react-router-dom'
import logo from '../../../assets/images/logo.png'
const InfoSection = () => {
    return (
        <div className="info-section">
            <div className="logo">
                <Link to="/"><img src={logo} alt="" /></Link>
            </div>
            <div className="info">
                <div className="contact">
                    <div><i className="fa fa-phone"></i></div>
                    <div>
                        <div>CALL SUPPORT</div>
                        <div>01-4361690 | 01-4970072</div>
                    </div>
                </div>
                <div className="email">
                    <div><i className="fa fa-envelope"></i></div>
                    <div>
                        <div>EMAIL SUPPORT</div>
                        <div>bibeklama67@gmail.com</div>
                    </div>
                </div>
                <div className="location">
                    <div><i className="fa fa-map-marker"></i></div>
                    <div>
                        <div>LOCATION</div>
                        <div>Nayabazar, Khusibun</div>
                    </div>
                </div>
            </div>
        </div>
    )
}
export default InfoSection