import React from 'react'
import { Link } from 'react-router-dom'
const TopBar = () => {
    return (
        <div className="header">
            <div className="info">
                <div className="tag-line">
                    <div>FEAR OF LORD IS BRGINNING OF WISDOM</div>
                </div>
                <div className="social-links">
                    <div>
                        <i className="fa fa-instagram"></i>
                    </div>
                    <div>
                        <i className="fa fa-youtube"></i>
                    </div>
                    <div>
                        <i className="fa fa-twitter"></i>
                    </div>
                    <div>
                        <i className="fa fa-facebook"></i>
                    </div>
                </div>
                <div className="quick-links">
                    <div>
                        <Link to="/admin-login">
                            QUICK LINKS <i className="fa fa-caret-down"></i>
                        </Link>
                    </div>
                </div>
            </div>
        </div>
    )
}
export default TopBar