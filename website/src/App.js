import React from 'react';
import './App.scss';
import { animateScroll as scroll } from 'react-scroll'
import { Route, Switch, useHistory } from "react-router-dom"
import MessageFromPrincipalView from './views/MessageFromPrincipalView';
import LandingView from './views/LandingView';
import Login from './sections/AdminView/Authentication/Login';
function App(props) {
  const history = useHistory();
  history.listen((location, action) => {
    scroll.scrollToTop();
  })
  return (
    <>
      <Switch>
        <Route path="/admin-login" name="admin-login" component={Login} exact />
        <Route path="/message-from-principal" name="message-from-principal" component={MessageFromPrincipalView} exact />
        <Route path="" name="landing" component={LandingView} exact />
      </Switch>
    </>
  )
}
export default (App);
