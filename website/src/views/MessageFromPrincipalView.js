import React from 'react'
import MessageFromPrincipal from '../sections/FrontView/MessageFromPrincipal'
import Footer from '../containers/footer'
import Header from '../containers/header'
import About from '../sections/FrontView/About'

const MessageFromPrincipalView = () => {
    return (
        <>
            <Header />
            <MessageFromPrincipal />
            <About />
            <Footer />
        </>
    )
}
export default MessageFromPrincipalView