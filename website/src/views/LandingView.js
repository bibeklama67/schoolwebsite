import React from 'react'
import Landing from '../sections/FrontView/Landing'
import Header from '../containers/header'
import Footer from '../containers/footer'
import News from '../sections/FrontView/News'
import Courses from '../sections/FrontView/Courses'
import About from '../sections/FrontView/About'
import Achievements from '../sections/FrontView/Achievements'
import VideoSection from '../sections/FrontView/VideoSection'
import Notice from '../sections/FrontView/Notice'
import Testimonail from '../sections/FrontView/Testimonial'
import Partners from '../sections/FrontView/Partners'
const LandingView = () => {
    return (
        <>
            <Header />
            <Landing />
            <News />
            <Courses />
            <About />
            <Achievements />
            <VideoSection />
            <Notice />
            <Testimonail />
            <Partners />
            <Footer />
        </>
    )
}
export default LandingView