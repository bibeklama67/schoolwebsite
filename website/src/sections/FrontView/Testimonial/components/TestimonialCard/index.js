import React from 'react'
import './style.scss'

const TestimonialCard = (props) => {
    return (
        <div className="testimonial-card">
            <div className="testimonial-desc">
                Frankly speaking, I too was confused about which college to join, but today I am really satisfied that I  joined Prime. Prime has not only provided quality education but has…
            </div>
            <div className="author">
                <div>
                    <img src={props.img} alt="" />
                </div>
                <div>
                    BINI MAHARJAN, BIM 6TH SEMESTER, COLLEGE TOPPER
                </div>
            </div>
        </div>
    )
}
export default TestimonialCard