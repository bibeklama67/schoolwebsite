import React from 'react'
import './style.scss'

const MessageFromPrincipal = () => {
    return (
        <div className="message-from-principal-div">
            <div className="message-from-principal-header">Message from Principal</div>
            <div className="message-from-principal">
                <div className="image">
                    <img src="https://www.prime.edu.np/wp-content/uploads/2017/01/Chairman_new-768x922.jpg" alt="handsome pic of principal" />
                </div>
                <div className="text">
                    <p>A quality education is essential for students to secure a good future for themselves; it can not only enhance the lives of the students, but also contribute to the overall development of our country. As a result, “quality education” has been declared as one of the 17 major goals of United Nation’s Sustainable Development Agenda 2030. At Prime College, we are providing a “quality education” to our students through a world-class academic program(s), as well as a wide-ranging co-curriculum activities design to help students develop their professional skills.</p>

                    <p>The collective effort and dedication of the Prime College Team has led to a continual progress and success of our institution. After establishing the first BIM program in Nepal less than two decades ago, we have since added six new academic programs to our college’s catalog, including a master’s level course in Management. As a Management and I.T. institution, Prime College has continuously placed a special emphasis on innovation, as well as encouraged creative thinking among our students under the supervision of our dedicated faculty members. In past couple of years alone Prime College has collaborated with national and international institutions and hosted virtual seminars, conference, ICT Meet Up, and Artificial Intelligence Training workshop. We are also working to upgrade the overall infrastructure of the college to continue to offer our students, faculty members, and staff a cutting-edge and conducive learning environment.</p>

                    <p>With support and collaboration from and between the students, faculty members, staff, parents, and other well-wishers, I am certain that Prime College will continue to progress and evolve as an ideal educational institution and a hub for future leaders and innovators.</p>
                    <p className="info">

                        <h4>- Prof. Dr. Sundar Shyam Bhakta Mathema</h4>
                        <p>Chairperson - Prime College / Prime Educational Inc.</p>

                        <h4>Qualifications:</h4>
                        <p>M.Ed.(TU), MS (USA) & PhD. (Education Evaluation from India)</p>

                        <h4>Years of Experience:</h4>
                        <p>45 years in Teaching</p>
                    </p>
                </div>
            </div>
        </div>
    )
}
export default MessageFromPrincipal