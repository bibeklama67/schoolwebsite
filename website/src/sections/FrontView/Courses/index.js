import React, { useEffect, useState } from 'react'
import './style.scss'
import CourseCard from './components/CourseCard'
import decoImg from '../../../assets/images/decoImg.png'
const Courses = () => {
    const [showing, setShowing] = useState([0, 1, 2]);
    const [selectedButton, setSelectedButton] = useState(1)
    const showSlides = () => {
        var i
        var slides = document.getElementsByClassName("course-card")
        if (slides.length) {
            for (i = 0; i < slides.length; i++) {
                slides[i].style.display = "none"
            }
            showing.map(item => {
                slides[item].style.display = "block"
            })
        }
    }
    useEffect(() => {
        showSlides()
    }, [showSlides])
    return (
        <div className="our-courses">
            <div className="header-section">
                <div className="deco">
                    <img src={decoImg} alt=""></img>
                </div>
                <div className="sub-title">CHOOSE THE COURSE</div>
                <div className="title">OUR COURSES</div>
            </div>
            <div className="course-cards">
                <CourseCard course={1} />
                <CourseCard course={2} />
                <CourseCard course={3} />
                <CourseCard course={4} />
                <CourseCard course={5} />
                <CourseCard course={6} />
                <CourseCard course={7} />
                <CourseCard course={8} />
                <CourseCard course={9} />

            </div>
            <div className="course-slider-buttons">
                <div onClick={() => { setShowing([0, 1, 2]); setSelectedButton(1) }} className={`${selectedButton === 1 ? 'selected' : ''}`}></div>
                <div onClick={() => { setShowing([3, 4, 5]); setSelectedButton(2) }} className={`${selectedButton === 2 ? 'selected' : ''}`}></div>
                <div onClick={() => { setShowing([6, 7, 8]); setSelectedButton(3) }} className={`${selectedButton === 3 ? 'selected' : ''}`}></div>
            </div>
        </div>
    )
}
export default Courses