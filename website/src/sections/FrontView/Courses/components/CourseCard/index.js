import React from 'react'
import mockImg from '../../../../../assets/images/MBS.jpg'
import './style.scss'
const CourseCard = (props) => {
    const { course } = props
    return (
        <div className="course-card">
            <div className="image-div">
                <img src={mockImg} alt="" />
            </div>
            <div className="course-card-title">MBS (MASTER OF BUSINESS STUDIES) {course}</div>
            <div className="course-card-desc">MASTER OF BUSINESS STUDIES is a two year ( 4 semester masters degree) program affiliated to Tribhuvan university...</div>
        </div>
    )
}
export default CourseCard