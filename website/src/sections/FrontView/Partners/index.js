import React from 'react'
import './style.scss'
import decoImg from '../../../assets/images/decoImg.png'
import p1 from '../../../assets/images/p1.png'
import p2 from '../../../assets/images/p2.png'
import p3 from '../../../assets/images/p3.png'
import p4 from '../../../assets/images/p4.jpg'
import p5 from '../../../assets/images/p5.png'
import p6 from '../../../assets/images/p6.png'
import PartnerCard from './components/PartnerCard'

const Partners = () => {
    return (
        <div className="partners">
            <div className="header-section">
                <div className="deco">
                    <img src={decoImg} alt=""></img>
                </div>
                <div className="sub-title">we collaborate with</div>
                <div className="title">our partners</div>
            </div>
            <div className="partner-cards">
                <PartnerCard img={p1}/>
                <PartnerCard img={p2}/>
                <PartnerCard img={p3}/>
                <PartnerCard img={p4}/>
                <PartnerCard img={p5}/>
                <PartnerCard img={p6}/>
               
            </div>
        </div>
    )
}
export default Partners