import React from 'react'
import './style.scss'
const PartnerCard = (props) => {
    return (
        <div className="partner-card">
            <img src={props.img ?? ''} alt=" " />
        </div>
    )
}
export default PartnerCard