import React, { useEffect } from 'react'
import './style.scss'
import imageone from '../../../assets/school/imageone.jpg'
import imagetwo from '../../../assets/school/imagetwo.jpg'

const Landing = () => {

    var slideIndex = 0
    var autosilde
    const showSlides = () => {
        var i
        var slides = document.getElementsByClassName("slides")
        if (slides.length) {
            for (i = 0; i < slides.length; i++) {
                slides[i].style.display = "none"
            }
            slideIndex++;
            if (slideIndex > slides.length) { slideIndex = 1 }
            slides[slideIndex - 1].style.display = "block"
            autosilde = setTimeout(showSlides, 7000)
        }
    }
    useEffect(() => {
        showSlides()
    }, [])
    const slideHandler = (type) => {
        window.clearTimeout(autosilde)
        var i
        var slides = document.getElementsByClassName("slides")
        if (slides.length) {
            for (i = 0; i < slides.length; i++) {
                slides[i].style.display = "none"
            }
            if (type === "next") {
                slideIndex++;
                if (slideIndex > slides.length) { slideIndex = 1 }
                slides[slideIndex - 1].style.display = "block"
            }
            else {
                slideIndex--;
                if (slideIndex <= 0) { slideIndex = 1 }
                slides[slideIndex - 1].style.display = "block"
            }
        }
    }
    return (
        <div className='landing-view'>
            <div className="next" onClick={() => slideHandler('next')}><i className="fa fa-chevron-right"></i></div>
            <div className="prev" onClick={() => slideHandler('prev')}><i className="fa fa-chevron-left"></i></div>
            <div className="slides">
                <div className="landing-overlay"></div>
                <img src={imageone} alt="" />
                <div className="text-area">
                    <div className="title">Saint Joseph Batch 2070</div>
                    <div className="sub-title">Inititate | Explore | Innovate</div>
                    <button className="button">Learn More</button>
                </div>
            </div>
            <div className="slides">
                <div className="landing-overlay"></div>
                <img src={imagetwo} alt="" />
                <div className="text-area">
                    <div className="title">Saint Joseph High School</div>
                    <div className="sub-title">Tarkeshwor-5 | Kathmandu | Nepal</div>
                    <button className="button">Learn More</button>
                </div>
            </div>
        </div>
    )
}
export default Landing