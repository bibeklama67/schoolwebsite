import React from 'react'
import './style.scss'
import decoImg from '../../../assets/images/decoImg.png'

const Achievements = () => {
    return (
        <div className="achievements">
            <div className="header-section">
                <div className="deco">
                    <img src={decoImg} alt=""></img>
                </div>
                <div className="sub-title">OUR SUCCESS STORY</div>
                <div className="title">OUR ACHIEVEMENTS</div>
            </div>
            <div className="content">
                <div className="item">
                    <div className="icon"><i className="fa fa-glass"></i></div>
                    <div className="number">50</div>
                    <div className="item-title">Winner Awards</div>
                </div>
                <div className="item">
                    <div className="icon"><i className="fa fa-star"></i></div>
                    <div className="number">5</div>
                    <div className="item-title">Academic Programs</div>
                </div>
                <div className="item">
                    <div className="icon"><i className="fa fa-user"></i></div>
                    <div className="number">15</div>
                    <div className="item-title">TU TOPPERS</div>
                </div>
                <div className="item">
                    <div className="icon"><i className="fa fa-plug"></i></div>
                    <div className="number">1500</div>
                    <div className="item-title">GRATUATESFROM 4 FACULTY</div>
                </div>
            </div>
        </div>
    )
}
export default Achievements