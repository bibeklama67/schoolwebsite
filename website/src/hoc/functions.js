export function showNews(selector, slideNewsIndex, autosildeNews) {
    var i
    var newsSlides = document.getElementsByClassName(selector)
    if (newsSlides.length) {
        for (i = 0; i < newsSlides.length; i++) {
            newsSlides[i].style.display = "none"
        }
        slideNewsIndex++;
        if (slideNewsIndex > newsSlides.length) { slideNewsIndex = 1 }
        newsSlides[slideNewsIndex - 1].style.display = "block"
        autosildeNews = setTimeout(showNews, 1000)
    }
}