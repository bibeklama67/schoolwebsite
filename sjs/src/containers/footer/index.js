import React from 'react'
import './style.scss'
import logo from '../../assets/images/sjslogo.png'
import footer_img from '../../assets/images/map.jpg'
const Footer = () => {
    return (
        <div className="footer-div">
            <div className="footer">
                <div className="info">
                    <div>
                        <div className="footer-title">Saint Joseph's High School Goldhunga</div>
                        <div className="footer-desc">
                            Prime College is the first IT enabled college of Nepal and has been actively involved in educational and social sector since its inception.
                        </div>
                        <div className="logo-div">
                            <img src={logo} alt="" />
                        </div>
                    </div>
                    <div className="footer-social-links">
                        <div className="footer-title">FOLLOW US</div>
                    </div>
                </div>

                <div className="contacts">
                    <div>
                        <div className="footer-title">OUR CONTACTS</div>
                        <div className="footer-contact-items">
                            <div className="footer-contact-item">
                                <div><i className="fa fa-map-marker"></i></div>
                                <div>Khusibun, Nayabazar</div>
                            </div>
                            <div className="footer-contact-item">
                                <div><i className="fa fa-phone"></i></div>
                                <div>0145265485/9818445068</div>
                            </div>
                            <div className="footer-contact-item">
                                <div><i className="fa fa-envelope"></i></div>
                                <div>Khusibun, Nayabazar</div>
                            </div>
                        </div>
                    </div>
                    <div>
                        <div className="footer-title">FIND OUR LOCATION</div>
                        <div className="footer-image-slider">
                            <img src={footer_img} alt="" />
                        </div>
                    </div>
                </div>
                <div className="recent-news">
                    <div className="footer-title">RECENT NEWS</div>
                    <div className="footer-news">
                        <div className="footer-news-title">ADMISSION OPEN FOR +2 LEVEL EDUCATION IN SCIENCE, MANAGEMENT, AND HUMANITIES AT PRIME INTERNATIONAL SCHOOL</div>
                        <div className="footer-news-desc">
                            Are you ready to pursue your higher-level education and build a strong academic base for your future? Prime International School,...
                        </div>
                    </div>
                    <div className="footer-news">
                        <div className="footer-news-title">ADMISSION OPEN FOR +2 LEVEL EDUCATION IN SCIENCE, MANAGEMENT, AND HUMANITIES AT PRIME INTERNATIONAL SCHOOL</div>
                        <div className="footer-news-desc">
                            Are you ready to pursue your higher-level education and build a strong academic base for your future? Prime International School,...
                        </div>
                    </div>
                    <div className="footer-news">
                        <div className="footer-news-title">ADMISSION OPEN FOR +2 LEVEL EDUCATION IN SCIENCE, MANAGEMENT, AND HUMANITIES AT PRIME INTERNATIONAL SCHOOL</div>
                        <div className="footer-news-desc">
                            Are you ready to pursue your higher-level education and build a strong academic base for your future? Prime International School,...
                        </div>
                    </div>
                    <div className="footer-news">
                        <div className="footer-news-title">ADMISSION OPEN FOR +2 LEVEL EDUCATION IN SCIENCE, MANAGEMENT, AND HUMANITIES AT PRIME INTERNATIONAL SCHOOL</div>
                        <div className="footer-news-desc">
                            Are you ready to pursue your higher-level education and build a strong academic base for your future? Prime International School,...
                        </div>
                    </div>
                </div>
            </div>
            <div className="copy-right">
                Copyright@prime_college_2017
            </div>

        </div>
    )
}
export default Footer