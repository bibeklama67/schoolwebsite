import React, { useState } from 'react'
import './style.scss'

const VideoSection = () => {
    const [autoPlay, setAutoPlay] = useState(0)
    return (
        <div className="video-section">
            <iframe title=" " className="iframe"
                src={`https://www.youtube.com/embed/tgbNymZ7vqY?autoplay=${autoPlay}&mute=1`}>
            </iframe>
            {
                autoPlay === 0 && <div className="desc">
                    <div className="video-subtitle">know about our college</div>
                    <div className="video-title">With Our online videos</div>
                    <div className="play-btn" onClick={() => setAutoPlay(1)}>
                        <div>
                            <i className="fa fa-play"></i>
                        </div>
                    </div>
                </div>
            }
        </div >
    )
}
export default VideoSection