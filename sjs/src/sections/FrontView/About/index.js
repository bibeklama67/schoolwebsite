import React, { useEffect, useState } from 'react'
import decoImg from '../../../assets/images/decoImg.png'
import './style.scss'
import librarayIcon from '../../../assets/images/library-icon.png'
import healthyLifeIcon from '../../../assets/images/healthy_life.png'
import locationIcon from '../../../assets/images/location-icon.png'
import educationIcon from '../../../assets/images/education.png'
const About = () => {
    const [showing, setShowing] = useState([0, 1]);
    const [selectedButton, setSelectedButton] = useState(1)
    const showSlides = () => {
        var i
        var slides = document.getElementsByClassName("column")
        if (slides.length) {
            for (i = 0; i < slides.length; i++) {
                slides[i].style.display = "none"
            }
            showing.map(item => {
                slides[item].style.display = "block"
            })
        }
    }
    useEffect(() => {
        showSlides()
    }, [showSlides])
    return (
        <div className="about">
            <div className="header-section">
                <div className="deco">
                    <img src={decoImg} alt=""></img>
                </div>
                <div className="sub-title">SEE WHAT WE PROVIDE</div>
                <div className="title">ABOUT US</div>
                <p>Prime College is the first IT enabled college of Nepal and has been actively involved in educational and social sector since its inception.</p>
            </div>
            <div className="content-div">
                <div className="content">
                    <div className="column">
                        <div>
                            <div className='about-content-header'>
                                <img src={librarayIcon} alt="" />
                                <div>STUDENT SUPPORT AND GUIDANCE</div>
                            </div>
                            <div>
                                The college Library is well-stocked with a large number of textbooks , upgraded per faculty and .....
                            </div>
                        </div>
                        <div>
                            <div className='about-content-header'>
                                <img src={educationIcon} alt="" />
                                <div>COLLEGE COUNCELLING PROGRAM</div>
                            </div>
                            <div>
                                The college Library is well-stocked with a large number of textbooks , upgraded per faculty and .....
                            </div>
                        </div>
                    </div>
                    <div className="column">
                        <div>
                            <div className='about-content-header'>
                                <img src={healthyLifeIcon} alt="" />
                                <div>CAREER DEVELOPMENT CENTER</div>
                            </div>
                            <div>
                                The college Library is well-stocked with a large number of textbooks , upgraded per faculty and .....
                            </div>
                        </div>
                        <div>
                            <div className='about-content-header'>
                                <img src={locationIcon} alt="" />
                                <div>cOLLEGE LABS</div>
                            </div>
                            <div>
                                The college Library is well-stocked with a large number of textbooks , upgraded per faculty and .....
                            </div>
                        </div>
                    </div>
                    <div className="column">
                        <div>
                            <div className='about-content-header'>
                                <img src={librarayIcon} alt="" />
                                <div>LIBRARY</div>
                            </div>
                            <div>
                                The college Library is well-stocked with a large number of textbooks , upgraded per faculty and .....
                            </div>
                        </div>
                        <div>
                            <div className='about-content-header'>
                                <img src={educationIcon} alt="" />
                                <div>EDUCATIONAL EXCELLENCE</div>
                            </div>
                            <div>
                                The college Library is well-stocked with a large number of textbooks , upgraded per faculty and .....
                            </div>
                        </div>
                    </div>
                    <div className="column">
                        <div>
                            <div className='about-content-header'>
                                <img src={healthyLifeIcon} alt="" />
                                <div>LIFE AT PRIME</div>
                            </div>
                            <div>
                                The college Library is well-stocked with a large number of textbooks , upgraded per faculty and .....
                            </div>
                        </div>
                        <div>
                            <div className='about-content-header'>
                                <img src={locationIcon} alt="" />
                                <div>CENTRAL LOCATION</div>
                            </div>
                            <div>
                                The college Library is well-stocked with a large number of textbooks , upgraded per faculty and .....
                            </div>
                        </div>
                    </div>
                    <div className="slider">
                        <div onClick={() => { setShowing([0, 1]); setSelectedButton(1) }} className={`${selectedButton === 1 ? 'selected' : ''}`}><i className="fa fa-arrow-left" aria-hidden="true"></i></div>
                        <div onClick={() => { setShowing([2, 3]); setSelectedButton(2) }} className={`${selectedButton === 2 ? 'selected' : ''}`}><i className="fa fa-arrow-right" aria-hidden="true"></i></div>
                    </div>
                </div>
            </div>
        </div>
    )
}
export default About