import React from 'react'
import './style.scss'
const ContactUs = () => {
    return (
        <div className="contact-us">
            <div className="contact-title">
                <div className="title">Contact Us</div>
                <div className="short-desc">Please feel free to contact us or visit our school premise</div>
            </div>
            <div className="contact-details">
                <div className="info">
                    <div>Contact Information</div>
                    <div>We will be pleased to hear you soon !!!</div>
                    <div><i className="fa fa-phone" aria-hidden="true"> + 041 85748569 / 9818445068</i></div>
                    <div><i className="fa fa-envelope" aria-hidden="true"></i> saintjosephschool@gmail.com</div>
                    <div>Contact Information</div>
                    <div>Contact Information</div>
                </div>
                <div className="location">This</div>
            </div>

        </div>
    )
}
export default ContactUs