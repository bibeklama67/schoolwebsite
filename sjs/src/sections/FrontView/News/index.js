import React, { useEffect } from 'react'
import './style.scss'

const News = () => {
    var slideIndex = 0
    var autosilde
    const showSlides = () => {
        var i
        var slides = document.getElementsByClassName("news-feed")
        if (slides.length) {
            for (i = 0; i < slides.length; i++) {
                slides[i].style.display = "none"
            }
            slideIndex++;
            if (slideIndex > slides.length) { slideIndex = 1 }
            slides[slideIndex - 1].style.display = "block"
            autosilde = setTimeout(showSlides, 7000)
        }
    }
    useEffect(() => {
        showSlides()
    }, [showSlides])
    const slideHandler = (type) => {
        window.clearTimeout(autosilde)
        var i
        var slides = document.getElementsByClassName("news-feed")
        if (slides.length) {
            for (i = 0; i < slides.length; i++) {
                slides[i].style.display = "none"
            }
            if (type === "next") {
                slideIndex++;
                if (slideIndex > slides.length) { slideIndex = 1 }
                slides[slideIndex - 1].style.display = "block"
            }
            else {
                slideIndex--;
                if (slideIndex <= 0) { slideIndex = slides.length }
                slides[slideIndex - 1].style.display = "block"
            }
        }
    }
    return (
        <div className="news">
            <div className="news-slider-title">News Update</div>
            <div className="news-feed">
                <div className="news-title">prime alumni registration form</div>
                <div className="news-description">PRIME COLLEGE ALUMNI , Dear student we would like to request</div>
            </div>
            <div className="news-feed">
                <div className="news-title">Saint joseph high school</div>
                <div className="news-description">PRIME COLLEGE ALUMNI , Dear student we would like to request</div>
            </div>
            <div className="news-feed">
                <div className="news-title">Admission open</div>
                <div className="news-description">PRIME COLLEGE ALUMNI , Dear student we would like to request</div>
            </div>
            <div className="news-slider">
                <div onClick={() => slideHandler('next')}> <i className="fa fa-chevron-up"></i></div>
                <div onClick={() => slideHandler('prev')}> <i className="fa fa-chevron-down"></i></div>
            </div>
        </div>
    )
}
export default News