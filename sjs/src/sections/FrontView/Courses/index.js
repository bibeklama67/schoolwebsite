import React from 'react'
import './style.scss'
import decoImg from '../../../assets/images/decoImg.png'
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import Slider from 'react-slick'
import CourseCard from './components/CourseCard'
import { settings } from '../globalVars'

const Courses = () => {
    return (
        <div className="course-wrapper">
            <div className="header-section">
                <div className="header-section">
                    <div className="deco">
                        <img src={decoImg} alt=""></img>
                    </div>
                    <div className="sub-title">SEE WHAT WE PROVIDE</div>
                    <div className="title">Our Courses</div>
                    <p>We provide various types of courses to enhance the knowledge of the students</p>
                </div>
            </div>
            <Slider {...settings}>
                <CourseCard />
                <CourseCard />
                <CourseCard />
                <CourseCard />
                <CourseCard />
            </Slider>
        </div>
    )
}
export default Courses