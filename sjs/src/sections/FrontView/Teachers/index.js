

import React from 'react'
import './style.scss'
import { teachers } from '../mockData'
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import Slider from 'react-slick'
import TeacherCard from './components/TeacherCard';
import { settings } from '../globalVars'

const Teachers = () => {
  return (
    <div className="teacher-wrapper">
      <h2>Our Teachers</h2>
      <Slider {...settings}>
        {teachers.map((teacher, index) => (
          <TeacherCard teacher={teacher} />
        ))}
      </Slider>
    </div>
  )
}
export default Teachers