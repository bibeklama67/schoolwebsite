import React from 'react'
import './style.scss'

const TeacherCard = (props) => {
    const { teacher } = props
    return (
        <div className="card-wrapper">
            <div className="card">
                <div className="card-image">
                    <img src={teacher.image} alt=""/>
                </div>
                <ul className="social-icons">
                    <li><a href="https://www.facebook.com/"><i className="fa fa-facebook"></i></a></li>
                    <li><a href="https://www.facebook.com/"><i className="fa fa-instagram"></i></a></li>
                    <li><a href="https://www.facebook.com/"><i className="fa fa-twitter"></i></a></li>
                    <li><a href="https://www.facebook.com/"><i className="fa fa-dribbble"></i></a></li>
                </ul>
                <div className="details">
                    <h2>{teacher.first_name} {teacher.last_name}<span className="job-title">{teacher.position}</span><span className="phone">{teacher.phone}</span></h2>
                </div>
            </div>
        </div>
    )
}
export default TeacherCard