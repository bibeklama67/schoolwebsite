import React from 'react'
import './style.scss'

const Notice = () => {
    return (
        <div className="notice">
            <div className="desc">
                <div className="notice-title">T.U ADMISSION FOR B.sc CSIT | BBA | BIM | BBS</div>
                <div className="notice-tag">Hurry Up Limited Seats are available</div>
            </div>
            <div className="enroll-btn">
                Enroll Now
            </div>
        </div>
    )
}
export default Notice