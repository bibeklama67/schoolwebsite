import React, { useState } from 'react'
import './style.scss'
const FaqCard = () => {
    const [desc, setDesc] = useState(false)
    return (
        <div className="faq-card">
            <div className="title-div">
                <div className="faq-title">is uniform compulsory ?</div>
                <div className={`plus-btn ${desc ? 'active' : ''}`} onClick={() => setDesc(!desc)}>+</div>
            </div>
            {
                desc && <div className="faq-description">
                    It is mandatory for all students who are enrolled in Prime College to wear the college uniform. Failure to adhere to the college uniform code will disqualify the students from attending the college.
           </div>
            }
        </div>
    )
}
export default FaqCard