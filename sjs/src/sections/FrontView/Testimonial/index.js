import React from 'react'
import FaqCard from './components/FaqCard'
import TestimonialCard from './components/TestimonialCard'
import testi1 from '../../../assets/images/testi1.jpg'
import './style.scss'

const Testimonial = () => {
    return (
        <div className="testimonials">
            <div className="faq">
                <div>
                    GENERAL FAQ
               </div>
                <FaqCard />
                <FaqCard />
            </div>
            <div className="testimonial">
                <div>TESTIMONIAL</div>
                <div className="testimonial-card">
                    <TestimonialCard img={testi1} />
                </div>
                <div className="testimonial-slider">
                    <div className="icons">
                        <div></div>
                        <div></div>
                    </div>
                </div>
            </div>
        </div>
    )
}
export default Testimonial