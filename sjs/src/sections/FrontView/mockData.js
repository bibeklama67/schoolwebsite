import bibek from '../../assets/images/bibek.jpg'
import amit from '../../assets/images/elon.jpg'
import mockImage from '../../assets/images/elon.jpg'
import jeff from '../../assets/images/jeff.jpg'
export const teachers = [
    {
        first_name: 'Bibek',
        last_name: 'Lama',
        position: 'Propriter',
        image: bibek,
        phone: '9818445068',
        fb_link: '',
        insta_link: ''
    },
    {
        first_name: 'Amit',
        last_name: 'Singh',
        position: 'Accountant',
        image: amit,
        phone: '9818445068',
        fb_link: '',
        insta_link: ''
    },
    {
        first_name: 'Jeff',
        last_name: 'Bezos',
        position: 'CEO',
        image: jeff,
        phone: '984521454',
        fb_link: '',
        insta_link: ''
    },
    {
        first_name: 'Amit',
        last_name: 'Singh',
        position: 'Propriter',
        image: mockImage,
        phone: '9818445068',
        fb_link: '',
        insta_link: ''
    },
    {
        first_name: 'Amit',
        last_name: 'Singh',
        position: 'Propriter',
        image: mockImage,
        phone: '9818445068',
        fb_link: '',
        insta_link: ''
    },
]