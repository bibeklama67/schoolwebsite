import React from 'react'
import './style.scss'
const Login = () => {
    return (
        <div className="login-div">
            <div className="login">
                <div className="login-title">Login</div>
                <div className="inputs">
                    <div className="animated-input-form">
                        <div className="input-group">
                            <input type="text" required />
                            <label for="username" className="name-label">
                                <span className="content-name">Username</span>
                            </label>
                        </div>
                    </div>
                    <div className="animated-input-form">
                        <div className="input-group">
                            <input type="text" required />
                            <label for="username" className="name-label">
                                <span className="content-name">Password</span>
                            </label>
                        </div>
                    </div>
                </div>
                <div className="login-btn-div">
                    <button className="login-btn">Login</button>
                    <button className="register-btn">Register</button>
                </div>
            </div>
        </div>
    )
}
export default Login