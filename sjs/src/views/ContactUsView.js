import React from 'react'
import Footer from '../containers/footer'
import Header from '../containers/header'
import ContactUs from '../sections/FrontView/ContactUs'
import Testimonial from '../sections/FrontView/Testimonial'

const ContactUsView = () => {
    return (
        <>
            <Header />
            <ContactUs />
            <Testimonial />
            <Footer />
        </>
    )
}
export default ContactUsView